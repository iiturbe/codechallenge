//
//  CodeChallengeProtocols.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/25/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit
import Foundation

protocol CodeChallengeNavigation {
  func configureNavigation()
}

extension CodeChallengeNavigation where Self: UIViewController {
  func configureNavigation() {
    let wine: UIColor = UIColor(red: 171.0/255.0, green: 0.0, blue: 40.0/255.0, alpha: 1.0)
    self.navigationController?.navigationBar.barTintColor = wine
    self.navigationController?.navigationBar.tintColor = UIColor.white
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
  }
}

// MARK: - Router
protocol CodeChallengeRouterProtocol {
  func frontViewForWindow() -> UIViewController?
  func displayImageView(with model: ResponseImageModel, from view: CodeChallengeViewProtocol)
}

// MARK: - View
protocol CodeChallengeViewProtocol {
  var presenter: CodeChallengePresenterProtocol? { get set }
  func hideKeyboard()
  func showLoader()
  func hideLoader()
  func setupForError()
  func reloadTable()
  func incrementPageNumber()
  func updateTableFooter(isLoading: Bool)
}

// MARK: - Presenter
protocol CodeChallengePresenterProtocol {
  var interactor: CodeChallengeInteractorProtocol? { get set }
  var router: CodeChallengeRouterProtocol? { get set }
  var view: CodeChallengeViewProtocol? { get set }
  func beginSearch(with text: String, in pageNumber: Int)
  func beginSearch(in pageNumber: Int)
  func updateFrontView(with models: [ResponseImageModel])
  func searchFailed()
  func numberOfRows() -> Int
  func getModelAt(_ row: Int) -> ResponseImageModel
  func didSelectImage(at row: Int)
}

// MARK: - Interactor
protocol CodeChallengeInteractorProtocol {
  var dataManager: CodeChallengeDataManagerProtocol? { get set }
  var presenter: CodeChallengePresenterProtocol? { get set }
  func beginSearch(with text: String, in pageNumber: Int)
  func updateFrontView(with models: [ResponseImageModel])
  func searchFailed()
  func setupModel(_ responseModel: ResponseDataModel)
}

// MARK: - DataManager
protocol CodeChallengeDataManagerProtocol {
  var interactor: CodeChallengeInteractorProtocol? { get set }
  func performImageModelSearch(with text: String, in pageNumber: Int)
  func performImageDownload(with model: [DataModel])
}

