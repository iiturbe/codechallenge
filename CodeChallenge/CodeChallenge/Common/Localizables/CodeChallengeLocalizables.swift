//
//  CodeChallengeLocalizables.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/27/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import Foundation

class CodeChallengeLocalizables {
  // MARK: - Properties
  var tableName: String = "Localizables"
  
  public enum LocalizableType: String {
    case searchBarPlaceholder = "search_bar_placeholder"
    case errorMessage = "search_error_message"
  }
  
  // MARK: - Methods
  func getLocalizable(_ type: LocalizableType) -> String {
    return NSLocalizedString(type.rawValue, tableName: tableName, bundle: Bundle.main, value: "", comment: "")
  }
}
