//
//  FrontViewController.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/24/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class FrontViewController: UIViewController, CodeChallengeNavigation {

  // MARK: - Properties
  @IBOutlet weak var topView: UIView!
  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var bottomLabel: UILabel!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
  
  private var pageNumber: Int = 1
  private var searchBar: UISearchBar?
  private var localizables: CodeChallengeLocalizables = CodeChallengeLocalizables()
  
  var indicator: NVActivityIndicatorView?
  var presenter: CodeChallengePresenterProtocol?
  
  let midSize: CGFloat = 60.0
  let maxSize: CGFloat = 120.0
  private let mainCellHeight: CGFloat = 300.0
  private var somethingsWrong: Bool = false
  private let errorCellIdentifier: String = "ErrorCell"
  private let imageCellIdentifier: String = "ImageCell"
  let wine: UIColor = UIColor(red: 171.0/255.0, green: 0.0, blue: 40.0/255.0, alpha: 1.0)
  
  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    configureNavigation()
    setupSearchBar()
    setupTableView()
    configureUI()
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    beginAnimation()
  }

  override func viewWillLayoutSubviews() {
    updateSearchBar()
  }
  
  // MARK: - Private methods
  private func setupSearchBar() {
    let width = self.view.bounds.width * 0.9
    let frame = CGRect(x: 0.0, y: 0.0, width: width, height: 20.0)
    let searchBar = UISearchBar(frame: frame)
    searchBar.placeholder = localizables.getLocalizable(.searchBarPlaceholder)
    self.searchBar = searchBar
    searchBar.delegate = self
    let item = UIBarButtonItem(customView: searchBar)
    self.navigationItem.leftBarButtonItem = item
  }
  
  private func updateSearchBar() {
    let width = self.view.bounds.width * 0.9
    let frame = CGRect(x: 0.0, y: 0.0, width: width, height: 20.0)
    guard let searchBar = self.searchBar else { return }
    searchBar.frame = frame
  }
  
  private func configureUI() {
    let indicator = NVActivityIndicatorView(frame: CGRect.zero, type: .ballSpinFadeLoader, color: wine, padding: 0.0)
    self.indicator = indicator
    self.view.addSubview(indicator)
    self.view.bringSubviewToFront(indicator)
    indicator.translatesAutoresizingMaskIntoConstraints = false
    indicator.widthAnchor.constraint(equalToConstant: maxSize).isActive = true
    indicator.heightAnchor.constraint(equalToConstant: maxSize).isActive = true
    indicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    indicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    indicator.isHidden = true
    self.navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
  private func setupTableView() {
    tableView.delegate = self
    tableView.dataSource = self
    tableView.isHidden = true
    tableView.separatorStyle = .none
    tableView.tableFooterView = UIView()
    let errorCellNib = UINib(nibName: imageCellIdentifier, bundle: nil)
    tableView.register(errorCellNib, forCellReuseIdentifier: imageCellIdentifier)
    let imageCellNib = UINib(nibName: errorCellIdentifier, bundle: nil)
    tableView.register(imageCellNib, forCellReuseIdentifier: errorCellIdentifier)
  }

  private func beginAnimation() {
    UIView.animate(withDuration: 1.5, animations: { [unowned self] in
      self.topLabel.alpha = 0.0
      self.bottomLabel.alpha = 0.0
    }) { (_) in
      self.topLabel.isHidden = true
      self.bottomLabel.isHidden = true
      UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: { [unowned self] in
        self.topViewHeightConstraint.constant = 0.0
        self.view.layoutIfNeeded()
      }) { (_) in
        self.tableView.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.searchBar?.becomeFirstResponder()
      }
    }
  }

  private func setupTableFooterLoader() {
    let frame = CGRect(x: 0.0, y: ((tableView.bounds.width / 2) - 30), width: midSize, height: midSize)
    let indicator = NVActivityIndicatorView(frame: frame, type: .pacman, color: wine, padding: 1.0)
    self.tableView.tableFooterView = indicator
    indicator.startAnimating()
  }
}

// MARK: - UISearchBarDelegate
extension FrontViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    guard let text = searchBar.text else { return }
    presenter?.beginSearch(with: text, in: self.pageNumber)
  }
  
  func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    pageNumber = 1
  }
}

// MARK: - UITableViewDelegate
extension FrontViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return somethingsWrong ? 1 : presenter?.numberOfRows() ?? 0
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return somethingsWrong ? tableView.bounds.height : mainCellHeight
    
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if somethingsWrong {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: errorCellIdentifier) as? ErrorCell else { return UITableViewCell() }
      cell.selectionStyle = .none
      return cell
    } else {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: imageCellIdentifier) as? ImageCell,
        let model = presenter?.getModelAt(indexPath.row)
      else { return UITableViewCell() }
      cell.configure(model.image, model.title)
      cell.selectionStyle = .none
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    presenter?.didSelectImage(at: indexPath.row)
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    hideKeyboard()
    let contentOffset = scrollView.contentOffset.y
    let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
    if contentOffset >= maximumOffset {
      presenter?.beginSearch(in: pageNumber)
    }
  }
}

// MARK: - CodeChallengeViewProtocol
extension FrontViewController: CodeChallengeViewProtocol {
  func showLoader() {
    somethingsWrong = false
    tableView.isHidden = true
    self.indicator?.isHidden = false
    self.indicator?.startAnimating()
    searchBar?.isUserInteractionEnabled = false
  }
  
  func hideLoader() {
    searchBar?.isUserInteractionEnabled = true
    self.indicator?.stopAnimating()
    self.indicator?.isHidden = true
    tableView.isHidden = false
  }
  
  func hideKeyboard() {
    self.searchBar?.endEditing(true)
  }

  func setupForError() {
    somethingsWrong = true
  }

  func reloadTable() {
    self.tableView.reloadData()
    self.tableView.setContentOffset(.zero, animated: true)
  }

  func incrementPageNumber() {
    pageNumber += 1
  }

  func updateTableFooter(isLoading: Bool) {
    if isLoading && !somethingsWrong {
      setupTableFooterLoader()
    } else {
      self.tableView.tableFooterView = UIView()
    }
  }
}
