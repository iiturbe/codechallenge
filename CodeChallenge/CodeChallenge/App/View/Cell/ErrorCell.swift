//
//  ErrorCell.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/27/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit

class ErrorCell: UITableViewCell {
  
  // MARK: - Properties
  @IBOutlet weak var messageLabel: UILabel!
  private var localizables: CodeChallengeLocalizables = CodeChallengeLocalizables()
  
  // MARK: - Methods
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  private func setupCell() {
    messageLabel.text = localizables.getLocalizable(.errorMessage)
  }
}
