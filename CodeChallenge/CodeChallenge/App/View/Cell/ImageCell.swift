//
//  ImageCell.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/27/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit

class ImageCell: UITableViewCell {

  // MARK: - Properties
  @IBOutlet weak var mainImage: UIImageView!
  @IBOutlet weak var mainTitle: UILabel!
  
  // MARK: - Methods
  override func awakeFromNib() {
    super.awakeFromNib()
  }

  func configure(_ image: UIImage, _ title: String) {
    mainImage.image = image
    mainTitle.text = title
  }
}
