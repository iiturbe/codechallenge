//
//  ImageViewController.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/27/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, CodeChallengeNavigation {

  // MARK: - Properties
  @IBOutlet weak var mainImage: UIImageView!
  var model: ResponseImageModel!

  // MARK: - Life cycle
  override func viewDidLoad() {
    super.viewDidLoad()
    configureNavigation()
    setupUI()
  }

  // MARK: - Private method
  private func setupUI() {
    self.title = model.title
    mainImage.image = model.image
  }
}
