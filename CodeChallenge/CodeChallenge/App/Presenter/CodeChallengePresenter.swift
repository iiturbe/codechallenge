//
//  CodeChallengePresenter.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/26/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import Foundation

class CodeChallengePresenter: CodeChallengePresenterProtocol {
  
  // MARK: - Properties
  var interactor: CodeChallengeInteractorProtocol?
  var router: CodeChallengeRouterProtocol?
  var view: CodeChallengeViewProtocol?
  
  private var imageModels: [ResponseImageModel] = []
  private var currentText: String?
  private var isSearching: Bool = false
  
  // MARK: - Methods
  func beginSearch(with text: String, in pageNumber: Int) {
    self.imageModels = []
    self.currentText = text
    view?.hideKeyboard()
    view?.showLoader()
    interactor?.beginSearch(with: text, in: pageNumber)
  }

  func beginSearch(in pageNumber: Int) {
    guard let text = currentText, !isSearching else { return }
    isSearching = true
    interactor?.beginSearch(with: text, in: pageNumber)
    view?.updateTableFooter(isLoading: isSearching)
  }
  
  func updateFrontView(with models: [ResponseImageModel]) {
    self.imageModels.append(contentsOf: models)
    if isSearching {
      view?.updateTableFooter(isLoading: isSearching)
    }
    view?.hideLoader()
    view?.reloadTable()
    view?.incrementPageNumber()
    isSearching = false
  }

  func searchFailed() {
    isSearching = false
    view?.setupForError()
    view?.hideLoader()
    view?.reloadTable()
    view?.updateTableFooter(isLoading: isSearching)
  }

  func numberOfRows() -> Int {
    return imageModels.count
  }

  func getModelAt(_ row: Int) -> ResponseImageModel {
    return imageModels[row]
  }

  func didSelectImage(at row: Int) {
    guard let view = self.view else { return }
    let model = imageModels[row]
    router?.displayImageView(with: model, from: view)
  }
}

