//
//  CodeChallengeInteractor.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/26/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import Foundation

class CodeChallengeInteractor: CodeChallengeInteractorProtocol {
  
  // MARK: - Properties
  var dataManager: CodeChallengeDataManagerProtocol?
  var presenter: CodeChallengePresenterProtocol?

  // MARK: - Methods
  func beginSearch(with text: String, in pageNumber: Int) {
    dataManager?.performImageModelSearch(with: text, in: pageNumber)
  }

  func updateFrontView(with models: [ResponseImageModel]) {
    presenter?.updateFrontView(with: models)
  }

  func searchFailed() {
    presenter?.searchFailed()
  }
  
  func setupModel(_ responseModel: ResponseDataModel) {
    let fullDescriptionModels = responseModel.data.compactMap { (data) -> DataModel? in
      guard let title = data.title, let images = data.images else { return nil }
      guard !title.isEmpty && !images.isEmpty else { return nil }
      return data
    }
    let onlyImagesModels = fullDescriptionModels.compactMap { (data) -> DataModel? in
      guard let image = data.images?.first, let type = image.type else { return nil }
      guard type == "image/jpeg" else { return nil }
      return data
    }
    // Limitated to work faster
    var index = 0
    let limit = 5
    var filterModels = [DataModel]()
    while index < limit {
      filterModels.append(onlyImagesModels[index])
      index += 1
    }
    dataManager?.performImageDownload(with: filterModels)
  }
}
