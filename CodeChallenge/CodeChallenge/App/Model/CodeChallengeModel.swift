//
//  CodeChallengeModel.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/26/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit
import Foundation

struct ResponseDataModel: Codable {
  var success: Bool
  var status: Int
  var data: [DataModel]
}

struct ResponseImageModel {
  var description: String?
  var title: String
  var image: UIImage
}

struct DataModel: Codable {
  var id: String?
  var title: String?
  var description: String?
  var link: String?
  var images: [DataImage]?
}

struct DataImage: Codable {
  var id: String?
  var title: String?
  var description: String?
  var type: String?
  var link: String?
}
