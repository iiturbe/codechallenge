//
//  CodeChallengeDataManager.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/26/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class CodeChallengeDataManager: CodeChallengeDataManagerProtocol {

  // MARK: - Properties
  var interactor: CodeChallengeInteractorProtocol?

  private let authorization: String = "CLIENT-ID 126701CD8332F32"
  private let baseURL: String = "https://api.imgur.com/3/gallery/search/time/"
  
  // MARK: - Methods
  func performImageModelSearch(with text: String, in pageNumber: Int) {
    guard let url = URL(string: baseURL + "\(pageNumber)") else { return }
    let headers = ["Authorization" : authorization,
                   "Content-Type": "application/json"]
    let parameters: [String: Any] = ["q": text]
    Alamofire.request(
      url,
      method: .get,
      parameters: parameters,
      encoding: URLEncoding.default,
      headers: headers).responseJSON { [unowned self] (response) in
      switch response.result {
      case .success:
        guard let data = response.data else { return }
        let jsonDecoder = JSONDecoder()
        do {
          let model = try jsonDecoder.decode(ResponseDataModel.self, from: data)
          guard !model.data.isEmpty else { self.interactor?.searchFailed(); return }
          self.interactor?.setupModel(model)
        } catch {
          self.interactor?.searchFailed()
        }
      case .failure:
        self.interactor?.searchFailed()
      }
    }
  }
  
  func performImageDownload(with model: [DataModel]) {
    DispatchQueue.global(qos: .userInitiated).async {
      var models: [ResponseImageModel] = []
      let dispatchGroup = DispatchGroup()
      
      for modelData in model {
        guard let title = modelData.title,
              let imageData = modelData.images?.first,
              let imageLink = imageData.link
        else { continue }
        dispatchGroup.enter()
        
        Alamofire.request(imageLink).responseImage { (response) in
          guard let data = response.data, let image = UIImage(data: data) else { return }
          let newModel = ResponseImageModel(description: modelData.description, title: title, image: image)
          models.append(newModel)
          dispatchGroup.leave()
        }
      }
      _ = dispatchGroup.wait(timeout: .now() + 60.0)
    
      DispatchQueue.main.async {
        self.interactor?.updateFrontView(with: models)
      }
    }
  }
    
}
