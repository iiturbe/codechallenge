//
//  CodeChallengeRouter.swift
//  CodeChallenge
//
//  Created by Jonathan on 5/24/19.
//  Copyright © 2019 The dude. All rights reserved.
//

import UIKit
import Foundation

class CodeChallengeRouter: CodeChallengeRouterProtocol {
  
  // MARK: - Properties

  private let storyboard = UIStoryboard(name: "CodeChallenge", bundle: nil)
  private let imageViewID = "ImageViewController"
  
  // MARK: - Methods
  func frontViewForWindow() -> UIViewController? {
    
    guard let navigation = storyboard.instantiateInitialViewController() as? UINavigationController,
      let fronView = navigation.topViewController as? FrontViewController
    else { return nil }
    
    var presenter: CodeChallengePresenterProtocol = CodeChallengePresenter()
    var interactor: CodeChallengeInteractorProtocol = CodeChallengeInteractor()
    var dataManaget: CodeChallengeDataManagerProtocol = CodeChallengeDataManager()
    
    presenter.router = self
    presenter.view = fronView
    presenter.interactor = interactor
    fronView.presenter = presenter
    interactor.presenter = presenter
    interactor.dataManager = dataManaget
    dataManaget.interactor = interactor
    
    return navigation
  }

  func displayImageView(with model: ResponseImageModel, from view: CodeChallengeViewProtocol) {
    guard let imageView = storyboard.instantiateViewController(withIdentifier: imageViewID) as? ImageViewController,
      let viewController = view as? UIViewController else { return }
    imageView.model = model
    viewController.navigationController?.pushViewController(imageView, animated: true)
  }
}
